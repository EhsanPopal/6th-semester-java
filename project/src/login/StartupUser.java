package login;

import custom_utilities.DBConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;


public class StartupUser implements Initializable {
    
    @FXML
    public ChoiceBox sex;
    @FXML
    public TextField age;
    @FXML
    public TextField name;
    @FXML
    public TextField lastName;
    @FXML
    public TextField phone;
    @FXML
    public TextField salary;
    @FXML
    public TextField email;
    @FXML
    public TextField password;
    @FXML
    public TextField confirmPassword;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        sex.getItems().addAll("male", "female");
    }
    
    public void saveUser() throws Exception{
//        try{
            if(!password.getText().equals(confirmPassword.getText())) throw new Exception("Password & confirm password must match!");
            DBConnection db = new DBConnection();
            String query = "insert into employee (age, salary, phone, name, sex, last_name, job_start) values (?,?,?,?,?,?,curdate())";
            PreparedStatement ps = db.con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(age.getText()));
            ps.setInt(2, Integer.parseInt(salary.getText()));
            ps.setString(3, phone.getText());
            ps.setString(4, name.getText());
            ps.setString(5, sex.getSelectionModel().getSelectedItem().toString());
            ps.setString(6, lastName.getText());
            ps.executeUpdate();
            
            ps = db.con.prepareStatement("SELECT id as id FROM health.employee");
            ResultSet rs = ps.executeQuery();
            int id = -1;
            if(rs.next()){
               id = rs.getInt("id");
            }
            else{
                id = 1;
            }
            
            query = "insert into user_table (email, password, employee_id) values (?,?,?)";
            ps = db.con.prepareStatement(query);
            ps.setString(1, email.getText());
            ps.setString(2, password.getText());
            ps.setInt(3, id);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "account created successfully!");
//        }
//        catch(Exception e){
//            JOptionPane.showMessageDialog(null, e);
//        }
    }
    
    
}
