package login;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;

public class StartupDBController implements Initializable {
    
    @FXML
    public TextField username;
    @FXML
    public TextField dbpassword;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    public void saveDBSetting(){
        try {
            File myFile = new File("databaseInfo.txt");
            myFile.createNewFile();
            FileWriter fw = new FileWriter(myFile);
            fw.write(username.getText()+"\n");
            fw.write(dbpassword.getText());
            fw.close();
            JOptionPane.showMessageDialog(null, "file written");
            custom_utilities.DBConnection.password = dbpassword.getText();
            custom_utilities.DBConnection.user = username.getText();
        } 
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
}
