package login;

import custom_utilities.DBConnection;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import dashboard.DashboardController;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.stage.Stage;
//login validator
import custom_utilities.LoginValidation;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javax.swing.JOptionPane;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;

public class LoginController implements Initializable {

    @FXML
    private TextField email;
    @FXML
    private PasswordField password;
    @FXML
    private Label errorMessage;
    @FXML
    private Label newUserText;
    @FXML
    private FontAwesomeIconView newUserIcon;
    
    public void handleLoginButton(ActionEvent actionEvent)throws Exception{
        LoginValidation lv = new LoginValidation(email.getText(), password.getText());
        if(lv.match()){
            Node  source = (Node)  actionEvent.getSource(); 
            Stage stage  = (Stage) source.getScene().getWindow();
            DashboardController dc = new DashboardController();
            dc.displayStage("ehsan");
            stage.close();
        }
        else{
            errorMessage.setVisible(true);
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try{
            BufferedReader br = new BufferedReader(new FileReader("databaseInfo.txt"));
            String line;
            ArrayList data = new ArrayList();
            while((line = br.readLine()) != null){
                data.add(line);
            }
            br.close();
            custom_utilities.DBConnection.user = data.get(0).toString();
            custom_utilities.DBConnection.password = data.get(1).toString();
            
            DBConnection db = new DBConnection();
            String query = "select * from user_table";
            PreparedStatement ps = db.con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                newUserText.setVisible(false);
                newUserIcon.setVisible(false);
            }
            ps.close();
            rs.close();
            db.disconnect();

        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, "database username and password are not set!");
        }
    }

    public void startupSetting() throws Exception{
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("startupDB.fxml"));
        Scene scene = new Scene(root);
        stage.getIcons().add(new Image("/imag/hospital-icone.png"));
        stage.setTitle("DB setting");
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void firstUser() throws Exception{
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("startupUser.fxml"));
        Scene scene = new Scene(root);
        stage.getIcons().add(new Image("/imag/hospital-icone.png"));
        stage.setTitle("User setting");
        stage.setScene(scene);
        stage.showAndWait();
        newUserIcon.setVisible(false);
        newUserText.setVisible(false);
    }
}
