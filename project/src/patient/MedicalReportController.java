package patient;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javax.swing.JOptionPane;

public class MedicalReportController implements Initializable {

    @FXML
    private Label patName;
    @FXML
    private Label fatherName;
    @FXML
    private Label patPhone;
    @FXML
    private Label age;
    @FXML
    private Label sex;
    @FXML
    private Label visitorName;
    @FXML
    private Label visitorPhone;
    @FXML
    private Label coronavirus;
    @FXML
    private Label HIV;
    @FXML
    private Label sugar;
    @FXML
    private Label blood;
    @FXML
    private Label malaria;
    @FXML
    private Label chlora;
    @FXML
    private Label dateIn;
    @FXML
    private Label dateOut;
    @FXML
    private Label date;
    @FXML
    private Label doctor;
    @FXML
    private Label prescroptionCost;
    @FXML
    private Label totalCharges;
    @FXML
    private Label testCost;
    @FXML
    private TextField filePath;
    @FXML
    private Label advice;
    
    public static Report report;
    private String path;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        cost
        int prescriptionTotal = 0;
        for (medicin.Medicin med : report.medicin){
            prescriptionTotal += med.getPrice();
        }
        prescroptionCost.setText(Integer.toString(prescriptionTotal));

        int testTotal = 0;
        if(!report.coronavirus.equalsIgnoreCase("Undefined") && !report.coronavirus.equals("")){
            testTotal += 200;
        }
        if(!report.chlora.equalsIgnoreCase("Undefined") && !report.coronavirus.equals("")){
            testTotal += 200;
        }
        if(!report.malaria.equalsIgnoreCase("Undefined") && !report.coronavirus.equals("")){
            testTotal += 200;
        }
        if(!report.HIV.equalsIgnoreCase("Undefined") && !report.coronavirus.equals("")){
            testTotal += 200;
        }
        if(!report.bloodP.equals("")){
            testTotal += 200;
        }
        if(!report.sugarLevel.equals("")){
            testTotal += 200;
        }
        testCost.setText(Integer.toString(testTotal));
        totalCharges.setText(Integer.toString(prescriptionTotal+testTotal+200));
        advice.setText(report.advice);
        patName.setText(report.name);
        fatherName.setText(report.fatherName);
        patPhone.setText(report.phone);
        age.setText(Integer.toString(report.age));
        sex.setText(report.sex);
        visitorName.setText(report.visitorName);
        visitorPhone.setText(report.visitorPhone);
        coronavirus.setText(report.coronavirus);
        malaria.setText(report.malaria);
        chlora.setText(report.chlora);
        sugar.setText(report.sugarLevel);
        blood.setText(report.bloodP);
        HIV.setText(report.HIV);
        dateIn.setText(report.dateIn);
        dateOut.setText(report.dateOut);
        date.setText(report.date);
        doctor.setText(report.doctorName);
    }
    
    public void choosePath(){
        DirectoryChooser dc = new DirectoryChooser();
        dc.setTitle("Save report");
        path = dc.showDialog(null).getAbsolutePath();
        filePath.setText(path);
    }
    
    public void saveFile(){
        try {
            Document document = new Document();            
            PdfWriter.getInstance(document, new FileOutputStream(path+"\\"+patName.getText()+date.getText()+".pdf"));
            document.open();
            Font titleFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 16, BaseColor.BLACK);
            Font secHeaderFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 10, BaseColor.BLACK);
            Font paraTextFont = FontFactory.getFont(FontFactory.TIMES, 10, BaseColor.DARK_GRAY);
            
            Paragraph header = new Paragraph("Health System MEEN - "+date.getText(), titleFont);
            
            Chunk patientHeader = new Chunk("Patient details:", secHeaderFont);
            Paragraph patientDetails = new Paragraph("Name:     "+patName.getText()+"\n", paraTextFont);
            patientDetails.add("Father name:     "+fatherName.getText()+"\n");
            patientDetails.add("Phone:     "+patPhone.getText()+"\n");
            patientDetails.add("Age:     "+age.getText()+"\n");
            patientDetails.add("Sex:     "+sex.getText()+"\n");
            patientDetails.add("Visitor name:     "+visitorName.getText()+"\n");
            patientDetails.add("Visitor phone:     "+visitorPhone.getText()+"\n");
            
            Chunk bedsteadHeader = new Chunk("\nBedstead details:", secHeaderFont);
            Paragraph bedsteadDetails = new Paragraph("Date-in:     "+dateIn.getText()+"\n", paraTextFont);
            bedsteadDetails.add("Date-out:     "+dateOut.getText()+"\n");
            
            Chunk doctorHeader = new Chunk("\nDoctor details:", secHeaderFont);
            Paragraph doctorDetails = new Paragraph("Name:     "+doctor.getText()+"\n", paraTextFont);
            
            Chunk testHeader = new Chunk("\nTest details:", secHeaderFont);
            Paragraph testDetails = new Paragraph("Coronavirus: "+coronavirus.getText()+" | ", paraTextFont);
            testDetails.add("Malaria: "+malaria.getText()+" | ");
            testDetails.add("Chlora: "+chlora.getText()+" | ");
            testDetails.add("HIV: "+HIV.getText()+" | ");
            testDetails.add("Sugar L: "+sugar.getText()+" | ");
            testDetails.add("BloodP: "+blood.getText()+" | ");
            
            Chunk doctorAdviceHeader = new Chunk("\nDoctor advice:", secHeaderFont);
            Paragraph doctorAdviceDetails = new Paragraph(advice.getText()+"\n", paraTextFont);
            
            Chunk expensesHeader = new Chunk("\nExpenses:", secHeaderFont);
            Paragraph expensesDetails = new Paragraph("\n", paraTextFont);
            expensesDetails.add("Prescription:     "+prescroptionCost.getText()+"\n");
            expensesDetails.add("Test(s):     "+testCost.getText()+"\n");
            expensesDetails.add("Total:     "+totalCharges.getText()+"\n");

            Chunk prescriptionHeader = new Chunk("\nPrescription:", secHeaderFont);
            Paragraph prescription = new Paragraph();
            prescription.setFont(paraTextFont);
            for(medicin.Medicin med:report.medicin){
                prescription.add(med.getName()+"   "+med.getDose()+"\n");
            }
            
            document.add(header);
            document.add(patientHeader);
            document.add(patientDetails);
            document.add(bedsteadHeader);
            document.add(bedsteadDetails);
            document.add(doctorHeader);
            document.add(doctorDetails);
            document.add(testHeader);
            document.add(testDetails);
            document.add(doctorAdviceHeader);
            document.add(doctorAdviceDetails);
            document.add(expensesHeader);
            document.add(expensesDetails);
            document.add(prescriptionHeader);
            document.add(prescription);
            document.close();
            JOptionPane.showMessageDialog(null, "report saved successfully!");
        } 
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
}
