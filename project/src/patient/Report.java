package patient;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import medicin.Medicin;

public class Report {
    public int id;
    public String name ="";
    public String fatherName="";
    public String sex="";
    public int age;
    public String phone="";
    public String date="";
    public String doctorName="";
    public String visitorName="";
    public String visitorPhone="";
    public String sugarLevel="";
    public String bloodP="";
    public String HIV="";
    public String malaria="";
    public String coronavirus="";
    public String chlora="";
    public String dateIn="";
    public String dateOut="";
    public String advice="";
    public int totalCost;
    
    public ObservableList<Medicin> medicin = FXCollections.observableArrayList();
}
