package patient;

public class Patient {
    public int billId;
    public int doctorId;
    public int id;
    public String name;
    public boolean sex;
    public String fatherName;
    public int age;
    public int phone;
    public int ward;
    public String date;
    public String gender;
    
    public Patient(int id,String name,String gender, int age, String fatherName, String date){
        this.age = age;
        this.id = id;
        this.fatherName = fatherName;
        this.date = date;
        this.name = name;
        this.gender = gender;
    }
    
    public boolean validateData(String billId, String doctorId, String id, String name, String sex, String fatherName, String age, String phone, String ward){
        try{
            this.age = Integer.parseInt(age);
            this.billId = Integer.parseInt(billId);
            this.id = Integer.parseInt(id);
            this.ward = Integer.parseInt(ward);
            this.sex = Boolean.parseBoolean(sex);
            this.name = name;
            this.fatherName = fatherName;
            this.phone = Integer.parseInt(phone);
            this.doctorId = Integer.parseInt(doctorId);
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
}
