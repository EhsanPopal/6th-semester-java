package patient;

import custom_utilities.DBConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import medicin.Medicin;

public class PatientController implements Initializable {
    
    @FXML
    private TextField name;
    @FXML
    private TextField fatherName;
    @FXML
    private TextField phone;
    @FXML
    private ChoiceBox doctor;
    @FXML
    private TextField age;
    @FXML
    private ChoiceBox sex;
    @FXML
    private DatePicker bedsteadDateIn;
    @FXML
    private DatePicker bedsteadDateOut;
    @FXML
    private CheckBox bedsteadActivator;
    @FXML
    private CheckBox visitorActivator;
    @FXML
    private TextField visitorName;
    @FXML
    private TextField visitorPhone;
    @FXML
    private CheckBox breportActivator;
    @FXML
    private ChoiceBox reportCoronavirus;
    @FXML
    private ChoiceBox reportHIV;
    @FXML
    private ChoiceBox reportCholora;
    @FXML
    private ChoiceBox reportMalaria;
    @FXML
    private TextField reportSugarLevel;
    @FXML
    private TextField reportBloodP;
    @FXML
    private Label operationMessage;
    @FXML
    private ChoiceBox medicinName;
    @FXML
    private TextField dose;
    @FXML
    private TableView<Medicin> prescriptionTable;
    @FXML
    private TextArea advice;
    
    private Boolean bedsteadState = true;
    private Boolean visitorState = true;
    private Boolean breportState = true;
    private ObservableList<Medicin> medicin = FXCollections.observableArrayList();
    private Report reportData = new Report();
    final private Bill bill=new Bill();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        operationMessage.setText("");
//        medicin table columns
        TableColumn<Medicin, String> idColumn = new TableColumn<>("ID");
        idColumn.setMinWidth(70);
        idColumn.setMaxWidth(70);
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Medicin, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setMinWidth(170);
        nameColumn.setMaxWidth(170);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        
        TableColumn<Medicin, String> priceColumn = new TableColumn<>("Dose");
        priceColumn.setMinWidth(75);
        priceColumn.setMaxWidth(75);
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("dose"));
        
        prescriptionTable.getColumns().addAll(idColumn, nameColumn, priceColumn);

//        medicin names
        new Thread(new Task() {
            @Override
            protected Void call() throws Exception {
                medicins();
                return null;
            }
        }).start();
        
        reportCoronavirus.getItems().addAll("Positive", "Negative", "Undefined");
        reportHIV.getItems().addAll("Positive", "Negative", "Undefined");
        reportCholora.getItems().addAll("Positive", "Negative", "Undefined");
        reportMalaria.getItems().addAll("Positive", "Negative", "Undefined");
        reportCholora.setValue("Undefined");
        reportHIV.setValue("Undefined");
        reportMalaria.setValue("Undefined");
        reportCoronavirus.setValue("Undefined");
                
//        doctor name setting
        new Thread(new Task() {
            @Override
            protected Void call() throws Exception {
                doctorListUpdate();
                return null;
            }
        }).start();
        
        
        sex.getItems().addAll("male", "female");
//        checkboxes setting
        bedsteadActivator.setSelected(true);
        visitorActivator.setSelected(true);
        breportActivator.setSelected(true);
    }
    
    public void bedsteadDetailsHandler(){
        bedsteadState = !bedsteadState;
        
//        changes the Pane state
        bedsteadDateIn.setDisable(bedsteadState);
        bedsteadDateOut.setDisable(bedsteadState);
    }
    
    public void visitorHandler(){
        visitorState = !visitorState;
        
        visitorName.setDisable(visitorState);
        visitorPhone.setDisable(visitorState);
    }
    
    public void breportHandler(){
        breportState = !breportState;
        
        reportCholora.setDisable(breportState);
        reportCoronavirus.setDisable(breportState);
        reportHIV.setDisable(breportState);
        reportMalaria.setDisable(breportState);
        reportSugarLevel.setDisable(breportState);
        reportBloodP.setDisable(breportState);
    }

    public void addUserLinkHandler()throws Exception{
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("../addUser/addUser.fxml"));
        Scene scene = new Scene(root);
        stage.getIcons().add(new Image("/imag/hospital-icone.png"));
        stage.setTitle("Add user");
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void addEmployeeLinkHandler()throws Exception{
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("../addEmployee/addEmployee.fxml"));
        Scene scene = new Scene(root);
         stage.getIcons().add(new Image("/imag/hospital-icone.png"));
        stage.setTitle("Add Employee");
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void addDoctorLinkHandler()throws Exception{
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("../addDoctor/addDoctor.fxml"));
        Scene scene = new Scene(root);
        stage.getIcons().add(new Image("/imag/hospital-icone.png"));
        stage.setTitle("Add doctor");
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void addNurseLinkHandler() throws Exception{
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("../addNurse/addNurse.fxml"));
        Scene scene = new Scene(root);
        stage.getIcons().add(new Image("/imag/hospital-icone.png"));
        stage.setTitle("Add nurse");
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void addWardLinkHandler() throws Exception{
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("../addWard/addWard.fxml"));
        Scene scene = new Scene(root);
        stage.getIcons().add(new Image("/imag/hospital-icone.png"));
        stage.setTitle("Add ward");
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void updatePatientLink()throws Exception{
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("updatePatient.fxml"));
        Scene scene = new Scene(root);
       
        stage.setTitle("Update patient");
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void addInfrastructureLink()throws Exception{
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("../addWard/addInfrastructure.fxml"));
        Scene scene = new Scene(root);
        stage.getIcons().add(new Image("/imag/hospital-icone.png"));
        stage.setTitle("Add infrastructure");
        stage.setScene(scene);
        stage.showAndWait();
    }
    
//    patient record save button
    public void savePatientButton()throws Exception{
        if(!name.getText().equals("")& !fatherName.getText().equals("")&sex.getSelectionModel().getSelectedItem()!=null & !phone.getText().equals("") & doctor.getSelectionModel().getSelectedItem() !=null & !age.getText().equals("")){
        final DBConnection db = new DBConnection();
        String query = "insert into health.patient (name, father_name, sex, age, phone, bedstead_id, date, advice) values (?,?,?,?,?,?,curdate(),?)";
        PreparedStatement ps = db.con.prepareStatement(query);
        ps.setString(1, name.getText());
        reportData.name = name.getText();
        ps.setString(2, fatherName.getText());
        reportData.fatherName = name.getText();
        ps.setString(3, sex.getSelectionModel().getSelectedItem().toString());
        reportData.sex = sex.getSelectionModel().getSelectedItem().toString();
        ps.setInt(4, Integer.parseInt(age.getText()));
        reportData.age = Integer.parseInt(age.getText());
        ps.setString(5, phone.getText());
        reportData.phone = phone.getText();
        ps.setObject(6, bedsteadState?null:bedsteadRecordCreator());
        ps.setString(7, advice.getText());
        reportData.advice = advice.getText();
        ps.executeUpdate();
        query = "select MAX(id) as id from patient";
        ps = db.con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        rs.next();
        final int id = rs.getInt("id");
        reportData.id = id;
        final int doctorId = getIdFromText(doctor.getSelectionModel().getSelectedItem().toString());
        reportData.doctorName = getNameFromText(doctor.getSelectionModel().getSelectedItem().toString());
        ps.close();
        rs.close();
        
//        doctor_patient query
        new Thread(new Task() {
            @Override
            protected Object call() throws Exception {
                String query = "INSERT INTO health.doctor_patient (patient_id, doctor_id) VALUES (?,?)";
                PreparedStatement pstatement = db.con.prepareStatement(query);
                pstatement.setInt(1, id);
                pstatement.setInt(2, doctorId);
                pstatement.executeUpdate();
                pstatement.close();
                return null;
            
            } }).start();
//        visitor query
        new Thread(new Task() {
            @Override
            protected Object call() throws Exception {
                if(!visitorState){
                    String query = "insert into visitor (name, phone, patient_id) values (?,?,?)";
                    PreparedStatement pstatement = db.con.prepareStatement(query);
                    pstatement.setString(1, visitorName.getText());
                    reportData.visitorName = visitorName.getText();
                    pstatement.setString(2, visitorPhone.getText());
                    reportData.visitorPhone = visitorPhone.getText();
                    pstatement.setInt(3, id);
                    pstatement.executeUpdate();
                    pstatement.close();
                }
                return null;
            }
        }).start();

//        report query
        new Thread(new Task() {
            @Override
            protected Object call() throws Exception {
                if(!breportState){
                    String query = "insert into report (suger, HIV, malaria, chlora, corona, patient_id, bloodP, date) values (?,?,?,?,?,?,?, curdate())";
                    PreparedStatement pps = db.con.prepareStatement(query);
                    pps.setString(1, reportSugarLevel.getText());
                    reportData.sugarLevel = reportSugarLevel.getText();
                    pps.setString(2, reportHIV.getSelectionModel().getSelectedItem().toString());
                    reportData.HIV = reportHIV.getSelectionModel().getSelectedItem().toString();
                    pps.setString(3, reportMalaria.getSelectionModel().getSelectedItem().toString());
                    reportData.malaria = reportMalaria.getSelectionModel().getSelectedItem().toString();
                    pps.setString(4, reportCholora.getSelectionModel().getSelectedItem().toString());
                    reportData.chlora = reportCholora.getSelectionModel().getSelectedItem().toString();
                    pps.setString(5, reportCoronavirus.getSelectionModel().getSelectedItem().toString());
                    reportData.coronavirus = reportCoronavirus.getSelectionModel().getSelectedItem().toString();
                    pps.setInt(6, id);
                    pps.setString(7, reportBloodP.getText());
                    reportData.bloodP = reportBloodP.getText();
                    pps.executeUpdate();
                    pps.close();
                }
                return null;
            }
        }).start();
        

//        saving prescription and medicins
        query = "INSERT INTO health.prescription (date, patient_id) VALUES (curdate(),?)";
        PreparedStatement pps= db.con.prepareStatement(query);
        pps.setInt(1, id);
        pps.executeUpdate();
        pps = db.con.prepareStatement("SELECT MAX(id) as id FROM health.prescription");
//                ResultSet rs = pps.executeQuery();
        rs = pps.executeQuery();
        rs.next();
        int prescriptionId = rs.getInt("id");
        rs.close();

        for (Medicin med : medicin) {
            query = "INSERT INTO health.medicin_prescription (prescription_id, medicin_id, dose) VALUES (?,?,?)";
            pps = db.con.prepareStatement(query);
            pps.setInt(1, prescriptionId);
            pps.setInt(2, med.getId());
            pps.setInt(3, Integer.parseInt(dose.getText()));
            pps.executeUpdate();
        }                
        pps.close();

        reportData.date = LocalDate.now().toString();
        reportData.medicin = medicin;
        MedicalReportController.report = reportData;
        
        //        bill query
        Thread billThread = new Thread(new Task() {
            @Override
            protected Object call() throws Exception {
                String query = "insert into bill (prescription, tests, patient_id, doctor_fee) values (?, ?, ?, ?)";
                PreparedStatement ps = db.con.prepareStatement(query);
                int testTotal = 0;
                if(!reportData.coronavirus.equalsIgnoreCase("Undefined") && !reportData.coronavirus.equals("")){
                    testTotal += 200;
                }
                if(!reportData.chlora.equalsIgnoreCase("Undefined") && !reportData.coronavirus.equals("")){
                    testTotal += 200;
                }
                if(!reportData.malaria.equalsIgnoreCase("Undefined") && !reportData.coronavirus.equals("")){
                    testTotal += 200;
                }
                if(!reportData.HIV.equalsIgnoreCase("Undefined") && !reportData.coronavirus.equals("")){
                    testTotal += 200;
                }
                if(!reportData.bloodP.equals("")){
                    testTotal += 200;
                }
                if(!reportData.sugarLevel.equals("")){
                    testTotal += 200;
                }
                ps.setInt(1, bill.prescriptionCharges);
                ps.setInt(2, testTotal);
                ps.setInt(3, id);
                ps.setInt(4, bill.doctorFee);
                ps.executeUpdate();
                ps.close();
                System.out.println("bill created");
                return null;
            }
        });
        billThread.start();
        billThread.join();
        
        db.disconnect();
        operationMessage.setText("Operation successful!");
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("medicalReport.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("medical report");
        stage.setScene(scene);
        stage.showAndWait();
        reportData = new Report();
        medicin = FXCollections.observableArrayList();
        prescriptionTable.getItems().clear();
    
    } else{
            JOptionPane.showMessageDialog(null,"please fill name, fathername, sex, age, doctor and phone fields");

}}
    public void getMedicinToTable() throws Exception{
        try {
            medicin.add(new Medicin(medicinName.getSelectionModel().getSelectedItem().toString(), Integer.parseInt(dose.getText()), getIdFromText(medicinName.getSelectionModel().getSelectedItem().toString()), getPriceFormText(medicinName.getSelectionModel().getSelectedItem().toString())*Integer.parseInt(dose.getText())));
            bill.prescriptionCharges += getPriceFormText(medicinName.getSelectionModel().getSelectedItem().toString())*Integer.parseInt(dose.getText());
            prescriptionTable.setItems(medicin);
        } 
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    public int getPriceFormText(String text){
        String arr[] = text.split("   ", 2);
        return Integer.parseInt(arr[1]);
    }
    
    private void doctorListUpdate() throws Exception{
        doctor.getItems().clear();
        String query = "SELECT D.id, E.name from health.doctor D join health.employee E on E.id = D.employee_id";
        DBConnection db = new DBConnection();
        PreparedStatement ps = db.con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            doctor.getItems().add(rs.getInt("id")+": "+rs.getString("name"));
        }
        ps.close();
        db.disconnect();
        rs.close();
    }
    
    private void medicins() throws Exception{
        medicinName.getItems().clear();
        String query = "SELECT id, name, price from medicin";
        DBConnection db = new DBConnection();
        PreparedStatement ps = db.con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            medicinName.getItems().add(rs.getInt("id")+": "+rs.getString("name")+"   "+rs.getInt("price"));
        }
        ps.close();
        db.disconnect();
        rs.close();
    }
    
    private int getIdFromText(String text){
        String arr[] = text.split(": ", 2);
        return Integer.parseInt(arr[0]);
    }
    
    private String getNameFromText(String text){
        String arr[] = text.split(": ", 2);
        return arr[1];
    }
    
    private int bedsteadRecordCreator(){
        try {
            reportData.dateIn = bedsteadDateIn.getValue().toString();
            reportData.dateOut = bedsteadDateOut.getValue().toString();
            DBConnection db = new DBConnection();
            String query = "insert into bedstead (in_date, out_date) values (curdate(), null)";
            PreparedStatement ps = db.con.prepareStatement(query);
            ps.executeUpdate();
            query = "select * from bedstead where id = (select MAX(id) from bedstead)";
            ps = db.con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                return rs.getInt("id");
            }
        } 
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        return 1;
    }
}

class Bill{
    int prescriptionCharges;
    int testCharges;
    int doctorFee = 200;
}