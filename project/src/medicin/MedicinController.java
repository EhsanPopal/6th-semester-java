package medicin;

import custom_utilities.DBConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class MedicinController implements Initializable {
    
    @FXML
    private TableView medicinTable;
    @FXML
    private TextField name;
    @FXML 
    private TextField price;
    @FXML
    private TextArea formula;
    @FXML
    private Label operationMessage;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        operationMessage.setText("");
        getMedicinToTable();
    }  
    
    
    
    @FXML
    public void updateMedicinLinkHandler()throws Exception{
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("updateMedicin.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Update medicin");
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void getMedicinToTable(){
        medicinTable.getColumns().clear();
//          name column
        TableColumn<SaveMedicin, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setMinWidth(200);
        nameColumn.setMaxWidth(200);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        
//        formula column
        TableColumn<SaveMedicin, String> formulaColumn = new TableColumn<>("Formula");
        formulaColumn.setMinWidth(240);
        formulaColumn.setMaxWidth(240);
        formulaColumn.setCellValueFactory(new PropertyValueFactory<>("formula"));
        
//        price column
        TableColumn<SaveMedicin, String> priceColumn = new TableColumn<>("Price");
        priceColumn.setMinWidth(80);
        priceColumn.setMaxWidth(80);
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        
        ObservableList<SaveMedicin> medicin = FXCollections.observableArrayList();
        try{
            DBConnection db = new DBConnection();
            String query = "select * from medicin";
            PreparedStatement ps = db.con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                medicin.add(new SaveMedicin(rs.getString("name"), rs.getString("formula"), rs.getInt("price")));
            }
            
            medicinTable.setItems(medicin);
            medicinTable.getColumns().addAll(nameColumn, formulaColumn, priceColumn);
            db.disconnect();
            ps.close();
            rs.close();
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    @FXML
    public void saveBtnHandler(){
        try{
            if(!name.getText().equals("") &!price.getText().equals("") &!formula.getText().equals("")){
            String query = "insert into health.medicin (name, formula, price) values (?,?,?)";
            DBConnection db = new DBConnection();
            PreparedStatement ps = db.con.prepareStatement(query);
            ps.setString(1, name.getText());
            ps.setString(2, formula.getText());
            ps.setInt(3, Integer.parseInt(price.getText()));
            ps.executeUpdate();
            db.disconnect();
            ps.close();
            operationMessage.setText("Operation successfully done!");
            getMedicinToTable();
        }
            else{JOptionPane.showMessageDialog(null, "Please fill all the blanks");
            }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
            operationMessage.setText("Operation failed!");
        }
    }
}
