package medicin;

public class SaveMedicin {
    private String name;
    private String formula;
    private int price;

    public SaveMedicin(String name, String formula, int price){
        this.name = name;
        this.price = price;
        this.formula = formula;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
    
}
