package addNurse;

import custom_utilities.DBConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class AddNurseController implements Initializable {

    @FXML
    private ChoiceBox employee;
    @FXML
    private ChoiceBox wards;
    @FXML
    private TextField profession;
    @FXML
    private Label operationMessage;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        operationMessage.setText("");
//        employee
        new Thread(new Task() {
            @Override
            protected Object call() throws Exception {
                updateEmployee();
                return null;
            }
        }).start();
//        wards
        new Thread(new Task() {
            @Override
            protected Object call() throws Exception {
                updateWards();
                return null;
            }
        }).start();
    } 
    public void start(Stage stage) throws Exception {
        Parent root = new Parent() {};
        stage.getIcons().add(new Image("/imag/hospital-icone.png"));
        Scene scene = new Scene(root);
        stage.setTitle("Login");
        stage.setScene(scene);
        stage.show();
    }
    private void updateEmployee() throws Exception{
        employee.getItems().clear();
        DBConnection db = new DBConnection();
        String query = "select E.id, E.name, E.last_name from employee E left join nurse N on N.employee_id = E.id left join doctor D on D.employee_id = E.id where D.id is null and N.id is null";
        PreparedStatement ps = db.con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            employee.getItems().add(rs.getInt("id")+": "+rs.getString("name")+" "+rs.getString("last_name"));
        }
        rs.close();
        ps.close();
        db.disconnect();
    }
    
    private void updateWards()throws Exception{
        wards.getItems().clear();
        DBConnection db = new DBConnection();
        String query = "select * from health.ward";
        PreparedStatement ps = db.con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            wards.getItems().add(rs.getInt("id")+": "+rs.getString("name"));
        }
        rs.close();
        ps.close();
        db.disconnect();
    }
    
    public void saveDoctorBtnHandler()throws Exception{
        try{
            if(employee.getSelectionModel().getSelectedItem() != null & wards.getSelectionModel().getSelectedItem() != null & !profession.getText().equals("")){
                String query = "insert into health.nurse (ward_id,profession, employee_id) values (?,?,?)";
                DBConnection db = new DBConnection();
                PreparedStatement ps = db.con.prepareStatement(query);
                ps.setInt(1, getIdFromText(wards.getSelectionModel().getSelectedItem().toString()));
                ps.setString(2, profession.getText());
                ps.setInt(3, getIdFromText(employee.getSelectionModel().getSelectedItem().toString()));
                ps.executeUpdate();
                ps.close();
                db.disconnect();
                profession.setText("");
                employee.getSelectionModel().clearSelection();
                wards.getSelectionModel().clearSelection();
                operationMessage.setText("Operation successful!");
                new Thread(new Task() {
                    @Override
                    protected Object call() throws Exception {
                        updateWards();
                        return null;
                    }
                }).start();
                new Thread(new Task() {
                    @Override
                    protected Object call() throws Exception {
                        updateEmployee();
                        return null;
                    }
                }).start();
            }
            else{
                 JOptionPane.showMessageDialog (null,"Employee, Profession or Ward can't be left blank!");
            }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
            operationMessage.setText("Operation failed!");
        }
    }
    
    private int getIdFromText(String text){
        String arr[] = text.split(": ", 2);
        return Integer.parseInt(arr[0]);
    }
}
