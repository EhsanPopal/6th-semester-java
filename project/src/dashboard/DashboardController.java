package dashboard;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class DashboardController implements Initializable {
    
    @FXML
    private AnchorPane contentPane;
    @FXML
    private VBox patientButton;
    @FXML
    private VBox medicinButton;
    @FXML
    private VBox reportButton;
    @FXML
    private VBox schedualButton;
    @FXML
    private VBox incedentButton;
    @FXML
    private Label pageLable;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            patientClickHandler();
        } catch (Exception ex) {
            Logger.getLogger(DashboardController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//    only called by LOGIN section
    public void displayStage(String username)throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("dashboard.fxml"));
        Stage stage = new Stage();
       stage.getIcons().add(new Image("/imag/hospital-icone.png"));
        stage.setTitle("Dashboard");
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    public void patientClickHandler()throws Exception{
        unselectAll();
        patientButton.setStyle("-fx-background-color: #f4f4f4;");
        pageLable.setText("Patients");
        Parent patientPane = FXMLLoader.load(getClass().getResource("../patient/patient.fxml"));
        
        contentPane.getChildren().clear();
        contentPane.getChildren().add(patientPane);
    }
    public void medicinClickHandler()throws Exception{
        unselectAll();
        medicinButton.setStyle("-fx-background-color: #f4f4f4;");
        pageLable.setText("Medicins");
        Parent patientPane = FXMLLoader.load(getClass().getResource("../medicin/medicin.fxml"));
        contentPane.getChildren().clear();
        contentPane.getChildren().add(patientPane);
    }
    public void reportClickHandler()throws Exception{
        unselectAll();
        reportButton.setStyle("-fx-background-color: #f4f4f4;");
        pageLable.setText("Reports");
        Parent reportPane = FXMLLoader.load(getClass().getResource("../report/report.fxml"));
        contentPane.getChildren().clear();
        contentPane.getChildren().add(reportPane);
    }
    public void schedualClickHandler()throws Exception{
        unselectAll();
        schedualButton.setStyle("-fx-background-color: #f4f4f4;");
        pageLable.setText("Schedual");
        Parent reportPane = FXMLLoader.load(getClass().getResource("../addEmployee/Schedual.fxml"));
        contentPane.getChildren().clear();
        contentPane.getChildren().add(reportPane);
    }
    public void incedentClickHandler()throws Exception{
        unselectAll();
        incedentButton.setStyle("-fx-background-color: #f4f4f4;");
        pageLable.setText("Incedents");
        Parent patientPane = FXMLLoader.load(getClass().getResource("../incedence/incedence.fxml"));
        contentPane.getChildren().clear();
        contentPane.getChildren().add(patientPane);
    }
    private void unselectAll(){
        patientButton.setStyle("-fx-background-color:  #ffffff;");
        medicinButton.setStyle("-fx-background-color:  #ffffff;");
        reportButton.setStyle("-fx-background-color:  #ffffff;");
        schedualButton.setStyle("-fx-background-color:  #ffffff;");
        incedentButton.setStyle("-fx-background-color:  #ffffff;");
    }
}
