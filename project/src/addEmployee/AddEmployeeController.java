package addEmployee;

import custom_utilities.DBConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javax.swing.JOptionPane;

public class AddEmployeeController implements Initializable {
    @FXML
    private TextField name;
    @FXML
    private TextField lastName;
    @FXML
    private TextField phone;
    @FXML
    private TextField salary;
    @FXML
    private TextField age;
    @FXML
    private ChoiceBox sex;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnUpdate;
    @FXML
    private Label operationMessage;
    @FXML
    private ChoiceBox selectEmployee;
    @FXML
    private Button btnDelete;
    
    ArrayList<Employee> employees = new ArrayList<>();
    int selectedEmployeeID = -1;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        operationMessage.setText("");
        sex.getItems().addAll("male", "female");
        
//        load patient records
        new Thread(new Task() {
            @Override
            protected Object call() throws Exception {
                DBConnection db = new DBConnection();
                String query = "select * from employee";
                PreparedStatement ps = db.con.prepareStatement(query);
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    employees.add(new Employee(rs.getInt(("id")),rs.getString("name"), rs.getString("phone"), rs.getString("sex"), rs.getInt("salary"), rs.getString("last_name"), rs.getInt("age")));
                    selectEmployee.getItems().add(rs.getInt("id")+": "+rs.getString("name")+" "+rs.getString("last_name"));
                }
                db.disconnect();
                ps.close();
                rs.close();
                return null;
            }
        }).start();
    }
    
    private boolean validatefields(){
    if(name.getText().isEmpty() | lastName.getText().isEmpty() | sex.getSelectionModel().isEmpty()){
         JOptionPane.showMessageDialog(null, "Please fill all the required fields");
        
        return false;
        }
      return true;
    }
    
    public void btnSaveHandler(){
        if(validatefields()){
        try{
            Employee employee = new Employee(name.getText(), phone.getText(), sex.getSelectionModel().getSelectedItem().toString(), Integer.parseInt(salary.getText()), lastName.getText(), Integer.parseInt(age.getText()));
            String query = "insert into health.employee (age, salary, phone, name, sex, last_name, job_start) values (?, ?, ?, ?, ?, ?, curdate())";
            DBConnection db = new DBConnection();
            PreparedStatement ps = db.con.prepareStatement(query);
            ps.setInt(1, employee.getAge());
            ps.setInt(2, employee.getSalary());
            ps.setString(3, employee.getPhone());
            ps.setString(4, employee.getName());
            ps.setString(5, employee.getSex());
            ps.setString(6, employee.getLastName());
            ps.executeUpdate();
            db.disconnect();
            ps.close();
            clear();
            operationMessage.setText("Operation successfull!");
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
            operationMessage.setText("Operation faild!");
        }
    }
    }
    private void clear(){
        name.setText("");
        lastName.setText("");
        phone.setText("");
        salary.setText("");
        age.setText("");
        operationMessage.setText("");
    }
    
    public void getEmployee(){
        selectedEmployeeID = selectEmployee.getSelectionModel().getSelectedItem() == null? -1 : getIdFromText(selectEmployee.getSelectionModel().getSelectedItem().toString());
        if(selectedEmployeeID != -1){
            Employee selectedEmployee = null;
            for(int i=0;i<employees.size();i++){
                if(selectedEmployeeID == employees.get(i).id){
                    selectedEmployee = employees.get(i);
                }
            }
            name.setText(selectedEmployee.getName());
            lastName.setText(selectedEmployee.getLastName());
            phone.setText(selectedEmployee.getPhone());
            salary.setText(Integer.toString(selectedEmployee.getSalary()));
            age.setText(Integer.toString(selectedEmployee.getAge()));
            sex.getSelectionModel().select(selectedEmployee.getSex());
            btnSave.setVisible(false);
            btnUpdate.setVisible(true);
            btnDelete.setVisible(true);
            
        }
    }
    
    private int getIdFromText(String text){
        String arr[] = text.split(": ", 2);
        return Integer.parseInt(arr[0]);
    }
    
    public void updateEmployee(){
       if(validatefields() & selectedEmployeeID != -1){
           try{
               DBConnection db = new DBConnection();
               String query = "update employee set age=?, salary=?, phone=?, name=?, sex=?, last_name=? where id=?";
               PreparedStatement ps = db.con.prepareStatement(query);
               ps.setInt(1, Integer.parseInt(age.getText()));
               ps.setInt(2, Integer.parseInt(salary.getText()));
               ps.setString(3, phone.getText());
               ps.setString(4, name.getText());
               ps.setString(5, sex.getSelectionModel().getSelectedItem().toString());
               ps.setString(6, lastName.getText());
               ps.setInt(7, selectedEmployeeID);
               ps.executeUpdate();
               db.disconnect();
               ps.close();
               clear();
               operationMessage.setText("Record updated!");
               btnUpdate.setVisible(false);
            btnDelete.setVisible(false);
           }
           catch(Exception e){
               JOptionPane.showMessageDialog(null, e);
               operationMessage.setText("Operation failed!");
           }
       }
       else{
           JOptionPane.showMessageDialog(null, "fill in the blank fields");
       }
    }
    public void deleteEmployee(){
       if(validatefields() & selectedEmployeeID != -1){
           try{
               DBConnection db = new DBConnection();
               String query = "delete from employee where id=?";
               PreparedStatement ps = db.con.prepareStatement(query);
               ps.setInt(1, selectedEmployeeID);
               ps.executeUpdate();
               db.disconnect();
               ps.close();
               clear();
               operationMessage.setText("Record deleteed!");
               btnUpdate.setVisible(false);
               btnDelete.setVisible(false);
           }
           catch(Exception e){
               JOptionPane.showMessageDialog(null, e);
               operationMessage.setText("Operation failed!");
           }
       }
       else{
           JOptionPane.showMessageDialog(null, "fill in the blank fields");
       }
    }
}
