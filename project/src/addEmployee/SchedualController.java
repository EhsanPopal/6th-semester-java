package addEmployee;

import custom_utilities.DBConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class SchedualController implements Initializable {
    
    @FXML
    private TableView schedualTable;
    
    ObservableList<Object> one = FXCollections.observableArrayList();
    ObservableList<Object> two = FXCollections.observableArrayList();
    ObservableList<Object> three = FXCollections.observableArrayList();
    ObservableList<Object> four = FXCollections.observableArrayList();
    ObservableList<Object> five = FXCollections.observableArrayList();
    ObservableList<Object> six = FXCollections.observableArrayList();
    ObservableList<Object> seven = FXCollections.observableArrayList();

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        getEnployeeToTable();
    }    
    
    private void getEnployeeToTable(){
//        table column
        TableColumn<Schedual, String> saturday = new TableColumn<>("saturday");
        saturday.setMinWidth(164);
        saturday.setCellValueFactory(new PropertyValueFactory<>("saturday"));
        
//        formula column
        TableColumn<Schedual, String> sunday = new TableColumn<>("Sunday");
        sunday.setMinWidth(164);
        sunday.setCellValueFactory(new PropertyValueFactory<>("sunday"));
        
//        price column
        TableColumn<Schedual, String> monday = new TableColumn<>("Monday");
        monday.setMinWidth(164);
        monday.setCellValueFactory(new PropertyValueFactory<>("monday"));
        
        TableColumn<Schedual, String> tuesday = new TableColumn<>("Teusday");
        tuesday.setMinWidth(164);
        tuesday.setCellValueFactory(new PropertyValueFactory<>("teusday"));
        
//        formula column
        TableColumn<Schedual, String> wednesday = new TableColumn<>("Wednesday");
        wednesday.setMinWidth(164);
        wednesday.setCellValueFactory(new PropertyValueFactory<>("wednesday"));
        
//        price column
        TableColumn<Schedual, String> thursday = new TableColumn<>("Thursday");
        thursday.setMinWidth(164);
        thursday.setCellValueFactory(new PropertyValueFactory<>("thursday"));
        
//        price column
        TableColumn<Schedual, String> friday = new TableColumn<>("Friday");
        friday.setMinWidth(165);
        friday.setCellValueFactory(new PropertyValueFactory<>("friday"));
//        the code bellow (in this section) is done by some ultra mind logic genius ;)
        try{
            final DBConnection db = new DBConnection();
//            saturday thread
            Thread sat = new Thread(() -> {
                String query = "select E.name, E.last_name, S.shift from employee E join schedual S on E.id = S.employee where S.week_day = 'SATURDAY'";
                try{
                    PreparedStatement ps = db.con.prepareStatement(query);
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        one.add(rs.getString("name")+" "+rs.getString("last_name")+" "+rs.getString("shift"));
                    }
                }
                catch(Exception e){
                    JOptionPane.showMessageDialog(null, e);
                }
            });
            sat.start();
//            sunday thread
            Thread sun = new Thread(() -> {
                String query = "select E.name, E.last_name, S.shift from employee E join schedual S on E.id = S.employee where S.week_day = 'SUNDAY'";
                try{
                    PreparedStatement ps = db.con.prepareStatement(query);
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        two.add(rs.getString("name")+" "+rs.getString("last_name")+" "+rs.getString("shift"));
                    }
                }
                catch(Exception e){
                    JOptionPane.showMessageDialog(null, e);
                }
            });
            sun.start();
//            monday thread
            Thread mon = new Thread(() -> {
                String query = "select E.name, E.last_name, S.shift from employee E join schedual S on E.id = S.employee where S.week_day = 'MONDAY'";
                try{
                    PreparedStatement ps = db.con.prepareStatement(query);
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        three.add(rs.getString("name")+" "+rs.getString("last_name")+" "+rs.getString("shift"));
                    }
                }
                catch(Exception e){
                    JOptionPane.showMessageDialog(null, e);
                }
            });
            mon.start();
//            teusday thread
            Thread teu = new Thread(() -> {
                String query = "select E.name, E.last_name, S.shift from employee E join schedual S on E.id = S.employee where S.week_day = 'TUESDAY'";
                try{
                    PreparedStatement ps = db.con.prepareStatement(query);
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        four.add(rs.getString("name")+" "+rs.getString("last_name")+" "+rs.getString("shift"));
                    }
                }
                catch(Exception e){
                    JOptionPane.showMessageDialog(null, e);
                }
            });
            teu.start();
//            wednesday thread
            Thread wed = new Thread(() -> {
                String query = "select E.name, E.last_name, S.shift from employee E join schedual S on E.id = S.employee where S.week_day = 'WEDNESDAY'";
                try{
                    PreparedStatement ps = db.con.prepareStatement(query);
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        five.add(rs.getString("name")+" "+rs.getString("last_name")+" "+rs.getString("shift"));
                    }
                }
                catch(Exception e){
                    JOptionPane.showMessageDialog(null, e);
                }
            });
            wed.start();
//            thursday thread
            Thread thu = new Thread(() -> {
                String query = "select E.name, E.last_name, S.shift from employee E join schedual S on E.id = S.employee where S.week_day = 'THURSDAY'";
                try{
                    PreparedStatement ps = db.con.prepareStatement(query);
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        six.add(rs.getString("name")+" "+rs.getString("last_name")+" "+rs.getString("shift"));
                    }
                }
                catch(Exception e){
                    JOptionPane.showMessageDialog(null, e);
                }
            });
            thu.start();
//            friday thread
            Thread fri = new Thread(() -> {
                String query = "select E.name, E.last_name, S.shift from employee E join schedual S on E.id = S.employee where S.week_day = 'FRIDAY'";
                try{
                    PreparedStatement ps = db.con.prepareStatement(query);
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        seven.add(rs.getString("name")+" "+rs.getString("last_name")+" "+rs.getString("shift"));
                    }
                }
                catch(Exception e){
                    JOptionPane.showMessageDialog(null, e);
                }
            });
            fri.start();
            
            sat.join();
            sun.join();
            mon.join();
            teu.join();
            wed.join();
            thu.join();
            fri.join();
            
            ObservableList schedualData = FXCollections.observableArrayList();
            int count = 0;
            if(one.size() > count) count = one.size();
            else if(two.size() > count) count = two.size();
            else if(three.size() > count) count = three.size();
            else if(four.size() > count) count = four.size();
            else if(five.size() > count) count = five.size();
            else if(six.size() > count) count = six.size();
            else if(seven.size() > count) count = seven.size();
            try{
                for(int i=0;i<count;i++){
                    schedualData.add(new Schedual(one.size()>i?one.get(i).toString():"", two.size()>i?two.get(i).toString():"", three.size()>i?three.get(i).toString():"", four.size()>i?four.get(i).toString():"", five.size()>i?five.get(i).toString():"", six.size()>i?six.get(i).toString():"", seven.size()>i?seven.get(i).toString():""));
                }
                schedualTable.getItems().clear();
                schedualTable.getItems().addAll(schedualData);
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, e);
            }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
        schedualTable.getColumns().addAll(saturday, sunday, monday, tuesday, wednesday, thursday, friday);
    }
    
    public void updateSchedualLinkHandler()throws Exception{
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("updateSchedual.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Update schedual");
        stage.setScene(scene);
        stage.showAndWait();
        one.clear();
        two.clear();
        three.clear();
        four.clear();
        five.clear();
        six.clear();
        seven.clear();
        getEnployeeToTable();
    }
}
