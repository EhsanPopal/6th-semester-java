package addEmployee;

import custom_utilities.DBConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javax.swing.JOptionPane;

public class UpdateSchedualController implements Initializable {

    @FXML
    private ChoiceBox employee;
    @FXML
    private ChoiceBox weekDay;
    @FXML
    private ChoiceBox shift;
    @FXML
    private Label operationMessage; 
    
    enum weekDays{
        SATURDAY,
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY
    }
    enum shifts{
        FIRST,
        SECOND
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        operationMessage.setText("");
        try{
            new Thread(new Task() {
                @Override
                protected Object call() throws Exception {
                    DBConnection db = new DBConnection();
                    String query = "select id, name, last_name from employee";
                    PreparedStatement ps = db.con.prepareStatement(query);
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        employee.getItems().add(rs.getInt("id")+": "+rs.getString("name")+" "+rs.getString("last_name"));
                    }
                    ps.close();
                    rs.close();
                    db.disconnect();
                    return null;
                }
            }).start();
            
            weekDay.getItems().addAll(weekDays.SATURDAY,weekDays.SUNDAY,weekDays.MONDAY,weekDays.TUESDAY,weekDays.WEDNESDAY,weekDays.THURSDAY,weekDays.FRIDAY);
            shift.getItems().addAll(shifts.FIRST, shifts.SECOND);
            shift.setValue("First");
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
    }    
    
    public void saveSchedualHandler(){
        try{
            DBConnection db = new DBConnection();
            String query ="insert into schedual (employee, shift, week_day) values (?, ?, ?)";
            PreparedStatement ps = db.con.prepareStatement(query);
            ps.setInt(1, getIdFromText(employee.getSelectionModel().getSelectedItem().toString()));
            ps.setString(2, shift.getSelectionModel().getSelectedItem().toString());
            ps.setString(3, weekDay.getSelectionModel().getSelectedItem().toString());
            ps.executeUpdate();
            db.disconnect();
            ps.close();
            operationMessage.setText("Operation successful!");
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    private int getIdFromText(String text){
        String arr[] = text.split(": ", 2);
        return Integer.parseInt(arr[0]);
    }
    
}
