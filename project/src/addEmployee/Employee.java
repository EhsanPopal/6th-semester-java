package addEmployee;

public class Employee {
    public int id;
    private String name;
    private String phone;
    private String sex;
    private int salary;
    private String lastName;
    private int age;
    private String jobStart;
    
    public Employee(String name, String phone, String sex, int salary, String lastName, int age){
        this.age = age;
        this.lastName = lastName;
        this.phone = phone;
        this.name = name;
        this.salary = salary;
        this.sex = sex;
    }
    
    public Employee(int id, String name, String phone, String sex, int salary, String lastName, int age){
        this.id = id;
        this.age = age;
        this.lastName = lastName;
        this.phone = phone;
        this.name = name;
        this.salary = salary;
        this.sex = sex;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    public void setJbStart(String jobStart){
        this.jobStart = jobStart;
    }
    
    public String getJobStart(){
        return this.jobStart;
    }
}
