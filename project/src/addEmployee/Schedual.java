package addEmployee;

public class Schedual {
    private String saturday = "";
    private String sunday = "";
    private String monday = "";
    private String teusday = "";
    private String wednesday = "";
    private String thursday = "";
    private String friday = "";

    public Schedual(String sat, String sun, String mon, String teu, String wed, String thu, String fri) {
        this.saturday = sat;
        this.sunday = sun;
        this.monday = mon;
        this.teusday = teu;
        this.wednesday = wed;
        this.thursday = thu;
        this.friday = fri;
    }
    
    public String getSaturday() {
        return saturday;
    }

    public void setSaturday(String saturday) {
        this.saturday = saturday;
    }

    public String getSunday() {
        return sunday;
    }

    public void setSunday(String sunday) {
        this.sunday = sunday;
    }

    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
        this.monday = monday;
    }

    public String getTuesday() {
        return teusday;
    }

    public void setTuesday(String tuesday) {
        this.teusday = tuesday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public void setWednesday(String wednesday) {
        this.wednesday = wednesday;
    }

    public String getThursday() {
        return thursday;
    }

    public void setThursday(String thursday) {
        this.thursday = thursday;
    }

    public String getFriday() {
        return friday;
    }

    public void setFriday(String friday) {
        this.friday = friday;
    }

    public String getTeusday() {
        return teusday;
    }

    public void setTeusday(String teusday) {
        this.teusday = teusday;
    }
    
    
}
