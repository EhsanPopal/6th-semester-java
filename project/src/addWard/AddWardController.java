package addWard;

import custom_utilities.DBConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javax.swing.JOptionPane;

public class AddWardController implements Initializable {
    @FXML
    private TextField name;
    @FXML
    private Label operationMessage;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        operationMessage.setText("");
    }
     private boolean validatefields(){
    if(name.getText().isEmpty()){
         JOptionPane.showMessageDialog(null, "Please fill all the required fields");
        
        return false;
        }
      return true;
    }
    public void btnSaveHandler(){
        if(validatefields()){
        try{
            String query = "insert into health.ward (name) values (?)";
            DBConnection db = new DBConnection();
            PreparedStatement ps = db.con.prepareStatement(query);
            ps.setString(1, name.getText());
            ps.executeUpdate();
            db.disconnect();
            ps.close();
            operationMessage.setText("Operation successful!");
            name.setText("");
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
            operationMessage.setText("Operation failed!");
        }
    }
}}
