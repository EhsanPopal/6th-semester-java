package addWard;

import custom_utilities.DBConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javax.swing.JOptionPane;

public class AddInfrastructureController implements Initializable {

    @FXML
    private TextField name;
    @FXML
    private TextField quantity;
    @FXML
    private ChoiceBox wards;
    @FXML
    private Label operationMessage;
            
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        operationMessage.setText("");
        new Thread(new Task() {
            @Override
            protected Object call() throws Exception {
                updateWards();
                return null;
            }
        }).start();
    }    
    
    private void updateWards()throws Exception{
        wards.getItems().clear();
        DBConnection db = new DBConnection();
        String query = "select * from health.ward";
        PreparedStatement ps = db.con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            wards.getItems().add(rs.getInt("id")+": "+rs.getString("name"));
        }
        rs.close();
        ps.close();
        db.disconnect();
    }
    
    public void saveBtnHandler(){
         
        try{
            if(wards.getSelectionModel().getSelectedItem() != null & !name.getText().equals("") & ! quantity.getText().equals("")){
            DBConnection db = new DBConnection();
            String query = "insert into infrastructure (name, ward_id, quantity, date) values (?,?,?, curdate())";
            PreparedStatement ps = db.con.prepareStatement(query);
            ps.setString(1, name.getText());
            ps.setInt(2, getIdFromText(wards.getSelectionModel().getSelectedItem().toString()));
            ps.setInt(3, Integer.parseInt(quantity.getText()));
            ps.executeUpdate();
            ps.close();
            db.disconnect();
            operationMessage.setText("Operation successful!");
            updateWards();
            quantity.setText("");
            name.setText("");
        }
         else{
          JOptionPane.showMessageDialog(null, "please fill all the blanks");
         }}
        catch(Exception e){
            operationMessage.setText("Operation failed!");
            JOptionPane.showMessageDialog(null, e);
        }
        
    }
    
    private int getIdFromText(String text){
        String arr[] = text.split(": ", 2);
        return Integer.parseInt(arr[0]);
    }
}
