package addUser;

import custom_utilities.DBConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javax.swing.JOptionPane;

public class AddUserController implements Initializable {
    @FXML
    private ChoiceBox employeeID;
    @FXML
    private TextField email;
    @FXML
    private TextField password;
    @FXML
    private TextField confirmPassword;
    @FXML
    private Label operationMessage;
    @FXML
    private ChoiceBox selectUser;
    
    int selectedUser = -1;
    ArrayList<User> users = new ArrayList<>();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        operationMessage.setText("");
        updateEmployeeIDs();
        
        new Thread(new Task() {
            @Override
            protected Object call() throws Exception {
                
                DBConnection db = new DBConnection();
                String query = "select * from user_table";
                PreparedStatement ps = db.con.prepareStatement(query);
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    selectUser.getItems().add(rs.getInt("id")+": "+rs.getString("email"));
                    users.add(new User(rs.getInt("id"), rs.getString("email"),rs.getString("password")));
                }
                return null;
            }
        }).start();
    }
    
    public void updateEmployeeIDs(){
        employeeID.getItems().clear();
        new Thread(new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try{
                    String query = "select E.id, E.name, E.last_name from employee E left join user_table U on E.id = U.employee_id where U.id is null";
                    DBConnection db = new DBConnection();
                    PreparedStatement ps = db.con.prepareStatement(query);
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        employeeID.getItems().add(rs.getInt("id")+": "+rs.getString("name")+" "+rs.getString("last_name"));
                    }
                }
                catch(Exception e){
                    JOptionPane.showMessageDialog(null, e);
                }
                return null;
            }
        }).start();
    }
    
    
    public void saveUserHandler(){
        String email = this.email.getText();
        String password = this.password.getText();
        String confirmPassword = this.confirmPassword.getText();
         int id = getIdFromText(this.employeeID.getSelectionModel().getSelectedItem().toString());
          if(this.employeeID.getSelectionModel().getSelectedItem()!= null & !this.email.getText().equals("") & !this.password.getText().equals("") & ! this.confirmPassword.getText().equals("")){
{                
            try{
                 if(password.equals(confirmPassword)){ 
                String query = "insert into health.user_table (email, password, employee_id) values (?,?,?);";
                DBConnection db = new DBConnection();
                PreparedStatement ps = db.con.prepareStatement(query);
                ps.setString(1, email);
                ps.setString(2, password);
                ps.setInt(3, id);
                ps.executeUpdate();
                db.disconnect();
                ps.close();
                updateEmployeeIDs();
                this.password.setText("");
                this.confirmPassword.setText("");
                this.email.setText("");
                operationMessage.setText("Operation successful!");
            }
                 else{  operationMessage.setText(" Password didnt match!");
           this.password.setText("");
           this.confirmPassword.setText("");;}
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, e);
            }
        
}}
        else{
           JOptionPane.showMessageDialog(null, "please fill all the blanks"); 
      }
    }
    
    private int getIdFromText(String text){
        String arr[] = text.split(": ", 2);
        return Integer.parseInt(arr[0]);
    }
    
    public void selectUserToFields(){
        selectedUser = selectUser.getSelectionModel().getSelectedItem() == null?-1:getIdFromText(selectUser.getSelectionModel().getSelectedItem().toString());
        if(selectedUser != -1){
            User user = null;
            for(int i=0;i<users.size();i++){
                if(selectedUser == users.get(i).id){
                    user = users.get(i);
                }
            }
            email.setText(user.email);
            password.setText(user.password);
        }
    }
    
    public void updateUser(){
        if(password.getText().equals(confirmPassword.getText())){
            try{
                DBConnection db = new DBConnection();
                String query = "update user_table set email=?, password=? where id=?";
                PreparedStatement ps = db.con.prepareStatement(query);
                ps.setString(1, email.getText());
                ps.setString(2, password.getText());
                ps.setInt(3, selectedUser);
                ps.executeUpdate();
                operationMessage.setText("User updated!");
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, e);
                operationMessage.setText("Operation failed!");
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "Password and Confirm Password should be same!");
        }
    }
    
    public void deleteUser(){
        if(password.getText().equals(confirmPassword.getText())){
            try{
                DBConnection db = new DBConnection();
                String query = "delete from user_table where id=?";
                PreparedStatement ps = db.con.prepareStatement(query);
                ps.setInt(1, selectedUser);
                ps.executeUpdate();
                operationMessage.setText("User delete!");
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, e);
                operationMessage.setText("Operation failed!");
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "Password and Confirm Password should be same!");
        }
    }
}

class User{
    int id;
    String email;
    String password;
    
    User(int id, String email, String password){
        this.id = id;
        this.email = email;
        this.password = password;
    }
}
