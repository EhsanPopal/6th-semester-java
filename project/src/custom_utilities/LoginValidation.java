package custom_utilities;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import login.User;

public class LoginValidation {
    private final User user = new User();
    
    public LoginValidation(String email, String password){// validation required for email
        user.setEmail(email);
        user.setPassword(password);
    }
    
    public boolean match()throws Exception{
        DBConnection db = new DBConnection();
        PreparedStatement ps = db.con.prepareStatement("select * from user_table where email=? and password=?");
        ps.setString(1, user.getEmail());
        ps.setString(2, user.getPassword());
        ResultSet rs = ps.executeQuery();
//        rs.next returns true if there are records obtained from the query and false otherwise
        while(rs.next()){
            ps.close();
            db.disconnect();
            return true;
        }
        ps.close();
        db.disconnect();
        return false;
    }
}
