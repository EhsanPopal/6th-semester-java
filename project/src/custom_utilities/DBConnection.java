package custom_utilities;

import com.smattme.MysqlExportService;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import javax.swing.JOptionPane;

public class DBConnection {
    public Connection con;
    public static String user;
    public static String password;
    
    Properties properties = new Properties();
    
    public DBConnection()throws Exception{
        properties.setProperty(MysqlExportService.DB_NAME, "health");
        properties.setProperty(MysqlExportService.DB_USERNAME, user);
        properties.setProperty(MysqlExportService.DB_PASSWORD, password);
        properties.setProperty(MysqlExportService.PRESERVE_GENERATED_ZIP, "true");
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/health?zeroDateTimeBehavior=convertToNull", user, password);
    }
    public void disconnect() throws SQLException{
        con.close();
    }
    
    public void dump(String path){
        try{
            MysqlExportService mysqlExportService = new MysqlExportService(properties);
            properties.setProperty(MysqlExportService.TEMP_DIR, new File(path+"\\HealthBackup").getPath());
            mysqlExportService.export();
            File file = mysqlExportService.getGeneratedZipFile();
            file.createNewFile();
            JOptionPane.showMessageDialog(null, "Backup successfully saved");
        }
        catch(IOException | ClassNotFoundException | SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }
    }
}
