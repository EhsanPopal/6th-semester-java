package report;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import custom_utilities.DBConnection;
import java.io.FileOutputStream;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.stage.DirectoryChooser;
import javax.swing.JOptionPane;
import patient.Patient;

public class ReportController implements Initializable {
    
    @FXML
    private ChoiceBox section;
    @FXML
    private DatePicker sectionFrom;
    @FXML
    private DatePicker sectionTo;
    @FXML
    private TextArea sectionNote;
    
    private ResultSet rs;
    private ObservableList data = FXCollections.observableArrayList();
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        section.getItems().addAll("patient", "incident", "doctor", "bill");
    }
    
    public void sectionExportHandler() throws Exception{
        try{
            String from = sectionFrom.getValue()==null?"":sectionFrom.getValue().toString();
            String to = sectionTo.getValue()==null?"":sectionTo.getValue().toString();
            DBConnection con = new DBConnection();
            
            switch(section.getSelectionModel().getSelectedItem().toString()){
                case "patient":
                    if(!from.equals("") && !to.equals("")){
                        String query = "select id, name, father_name, date, age, sex from patient where date > ? and date <= ?";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        ps.setString(1, from);
                        ps.setString(2, to);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new Patient(rs.getInt("id"), rs.getString("name"), rs.getString("sex"), rs.getInt("age"), rs.getString("father_name"), rs.getString("date")));
                        }
                        rs.close();
                        ps.close();
                    }
                    else if(!from.equals("")){
                        String query = "select id, name, father_name, date, age, sex from patient where date > ?";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        ps.setString(1, from);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new Patient(rs.getInt("id"), rs.getString("name"), rs.getString("sex"), rs.getInt("age"), rs.getString("father_name"), rs.getString("date")));
                        }
                        rs.close();
                        ps.close();
                    }
                    else if(!to.equals("")){
                        String query = "select id, name, father_name, date, age, sex from patient where date <= ?";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        ps.setString(1, to);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new Patient(rs.getInt("id"), rs.getString("name"), rs.getString("sex"), rs.getInt("age"), rs.getString("father_name"), rs.getString("date")));
                        }
                        rs.close();
                        ps.close();
                    }
                    else{
                        String query = "select id, name, father_name, date, age, sex from patient";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new Patient(rs.getInt("id"), rs.getString("name"), rs.getString("sex"), rs.getInt("age"), rs.getString("father_name"), rs.getString("date")));
                        }
                        rs.close();
                        ps.close();
                    }
                    break;
                case "incident":
                    if(!from.equals("") && !to.equals("")){
                        String query = "select P.id Patient_id, I.id Incident_id, P.name, P.father_name, I.date from incident I join patient P on I.patient_id = P.id where I.date > ? and I.date <= ?";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        ps.setString(1, from);
                        ps.setString(2, to);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new IncidentObject(rs.getInt("Patient_id"), rs.getInt("Incident_id"), rs.getString("name"), rs.getString("father_name"), rs.getString("date")));
                        }
                        rs.close();
                        ps.close();
                    }
                    else if(!from.equals("")){
                        String query = "select P.id Patient_id, I.id Incident_id, P.name, P.father_name, I.date from incident I join patient P on I.patient_id = P.id where I.date > ?";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        ps.setString(1, from);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new IncidentObject(rs.getInt("Patient_id"), rs.getInt("Incident_id"), rs.getString("name"), rs.getString("father_name"), rs.getString("date")));
                        }
                        rs.close();
                        ps.close();
                    }
                    else if(!to.equals("")){
                        String query = "select P.id Patient_id, I.id Incident_id, P.name, P.father_name, I.date from incident I join patient P on I.patient_id = P.id where I.date <= ?";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        ps.setString(1, to);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new IncidentObject(rs.getInt("Patient_id"), rs.getInt("Incident_id"), rs.getString("name"), rs.getString("father_name"), rs.getString("date")));
                        }
                        rs.close();
                        ps.close();
                    }
                    else{
                        String query = "select P.id Patient_id, I.id Incident_id, P.name, P.father_name, I.date from incident I join patient P on I.patient_id = P.id";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new IncidentObject(rs.getInt("Patient_id"), rs.getInt("Incident_id"), rs.getString("name"), rs.getString("father_name"), rs.getString("date")));
                        }
                        rs.close();
                        ps.close();
                    }
                    break;
                case "doctor":
                    if(!from.equals("") && !to.equals("")){
                        String query = "select E.id as Emp_id, D.id Doc_id, E.name, E.last_name, W.name as ward_name, (select count(*) from patient P join doctor_patient DP on P.id = DP.patient_id join doctor D on D.id = DP.doctor_id where D.id = Doc_id and P.date > ? and P.date <= ?) as patient_count from doctor D join Employee E on E.id = D.employee_id join ward W on W.id = D.ward_id";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        ps.setString(1, from);
                        ps.setString(2, to);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new DoctorObject(rs.getInt("Emp_id"), rs.getInt("Doc_id"), rs.getString("name"), rs.getString("last_name"), rs.getString("ward_name"), rs.getInt("patient_count")));
                        }
                        rs.close();
                        ps.close();
                    }
                    else if(!from.equals("")){
                        String query = "select E.id as Emp_id, D.id Doc_id, E.name, E.last_name, W.name as ward_name, (select count(*) from patient P join doctor_patient DP on P.id = DP.patient_id join doctor D on D.id = DP.doctor_id where D.id = Doc_id and P.date > ?) as patient_count from doctor D join Employee E on E.id = D.employee_id join ward W on W.id = D.ward_id";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        ps.setString(1, from);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new DoctorObject(rs.getInt("Emp_id"), rs.getInt("Doc_id"), rs.getString("name"), rs.getString("last_name"), rs.getString("ward_name"), rs.getInt("patient_count")));
                        }
                        rs.close();
                        ps.close();
                    }
                    else if(!to.equals("")){
                        String query = "select E.id as Emp_id, D.id Doc_id, E.name, E.last_name, W.name as ward_name, (select count(*) from patient P join doctor_patient DP on P.id = DP.patient_id join doctor D on D.id = DP.doctor_id where D.id = Doc_id and P.date <= ?) as patient_count from doctor D join Employee E on E.id = D.employee_id join ward W on W.id = D.ward_id";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        ps.setString(1, to);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new DoctorObject(rs.getInt("Emp_id"), rs.getInt("Doc_id"), rs.getString("name"), rs.getString("last_name"), rs.getString("ward_name"), rs.getInt("patient_count")));
                        }
                        rs.close();
                        ps.close();
                    }
                    else{
                        String query = "select E.id as Emp_id, D.id Doc_id, E.name, E.last_name, W.name as ward_name, (select count(*) from patient P join doctor_patient DP on P.id = DP.patient_id join doctor D on D.id = DP.doctor_id where D.id = Doc_id) as patient_count from doctor D join Employee E on E.id = D.employee_id join ward W on W.id = D.ward_id";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new DoctorObject(rs.getInt("Emp_id"), rs.getInt("Doc_id"), rs.getString("name"), rs.getString("last_name"), rs.getString("ward_name"), rs.getInt("patient_count")));
                        }
                        rs.close();
                        ps.close();
                    }
                    break;
                case "bill":
                    if(!from.equals("") && !to.equals("")){
                        String query = "select P.id, P.name, B.id as bill_id, B.prescription+B.tests+B.doctor_fee as total, P.date from patient P join bill B on P.id = B.patient_id where P.date > ? and P.date <= ?";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        ps.setString(1, from);
                        ps.setString(2, to);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new BillObject(rs.getInt("id"), rs.getString("name"), rs.getInt("bill_id"), rs.getInt("total"), rs.getString("date")));
                        }
                        rs.close();
                        ps.close();
                    }
                    else if(!from.equals("")){
                        String query = "select P.id, P.name, B.id as bill_id, B.prescription+B.tests+B.doctor_fee as total, P.date from patient P join bill B on P.id = B.patient_id where P.date > ?";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        ps.setString(1, from);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new BillObject(rs.getInt("id"), rs.getString("name"), rs.getInt("bill_id"), rs.getInt("total"), rs.getString("date")));
                        }
                        rs.close();
                        ps.close();
                    }
                    else if(!to.equals("")){
                        String query = "select P.id, P.name, B.id as bill_id, B.prescription+B.tests+B.doctor_fee as total, P.date from patient P join bill B on P.id = B.patient_id where P.date <= ?";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        ps.setString(1, to);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new BillObject(rs.getInt("id"), rs.getString("name"), rs.getInt("bill_id"), rs.getInt("total"), rs.getString("date")));
                        }
                        rs.close();
                        ps.close();
                    }
                    else{
                        String query = "select P.id, P.name, B.id as bill_id, B.prescription+B.tests+B.doctor_fee as total, P.date from patient P join bill B on P.id = B.patient_id";
                        PreparedStatement ps = con.con.prepareStatement(query);
                        rs = ps.executeQuery();
                        while(rs.next()){
                            data.add(new BillObject(rs.getInt("id"), rs.getString("name"), rs.getInt("bill_id"), rs.getInt("total"), rs.getString("date")));
                        }
                        rs.close();
                        ps.close();
                    }
                    break;
            }   
            
//            choose path for PDF file
            DirectoryChooser dc = new DirectoryChooser();
            dc.setTitle("Save report");
            String path = dc.showDialog(null).getAbsolutePath();
            
//            create PDF file
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(path+"\\"+section.getSelectionModel().getSelectedItem().toString()+LocalDate.now().toString()+"-"+LocalTime.now().getHour()+LocalTime.now().getMinute()+LocalTime.now().getSecond()+".pdf"));
            document.open();
            Font titleFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 16, BaseColor.BLACK);
            Font secHeaderFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 10, BaseColor.BLACK);
            Font paraTextFont = FontFactory.getFont(FontFactory.TIMES, 10, BaseColor.DARK_GRAY);
            Font noteFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 10, BaseColor.RED);
            
            Paragraph header = new Paragraph("Health System MEEN - "+LocalDate.now().toString()+" - "+LocalTime.now(), titleFont);
            
            Chunk detailsHeader = new Chunk(section.getSelectionModel().getSelectedItem().toString().toUpperCase()+" records from: "+from+", to: "+to+";", secHeaderFont);
            Chunk countHeader = new Chunk("\nThere are totally "+data.size()+" records.\nNote: ", secHeaderFont);
            
            Paragraph noteText = new Paragraph(sectionNote.getText(),noteFont);

            
            document.add(header);
            document.add(detailsHeader);
            document.add(countHeader);
            document.add(noteText);
            
            
            if(section.getSelectionModel().getSelectedItem().toString().equals("patient")){
                Paragraph patientDetails = new Paragraph("\n", paraTextFont);
                for(int i=0;i<data.size();i++){
                    Patient pat = (Patient) data.get(i);
                    patientDetails.add(pat.id+" - "+pat.name+" - "+pat.fatherName+" - "+pat.age+" - "+pat.sex+" - "+pat.date+"\n");
                }
                document.add(patientDetails);
            }
            else if(section.getSelectionModel().getSelectedItem().toString().equals("doctor")){
                Paragraph doctorDetails = new Paragraph("\n", paraTextFont);
                for(int i=0;i<data.size();i++){
                    DoctorObject doc = (DoctorObject) data.get(i);
                    doctorDetails.add(doc.d_id+" - "+doc.name+" - "+doc.last_name+" - "+doc.ward_name+" - "+doc.count+"(patients)\n");
                }
                document.add(doctorDetails);
            }
            else if(section.getSelectionModel().getSelectedItem().toString().equals("incident")){
                Paragraph incidentDetails = new Paragraph("\n", paraTextFont);
                for(int i=0;i<data.size();i++){
                    IncidentObject inc = (IncidentObject) data.get(i);
                    incidentDetails.add(inc.patientId+" - "+inc.name+" - "+inc.fatherName+" - "+inc.date+" - "+inc.incidentId+"(incident ID)\n");
                }
                document.add(incidentDetails);
            }
            else if(section.getSelectionModel().getSelectedItem().toString().equals("bill")){
                Paragraph billDetails = new Paragraph("\n", paraTextFont);
                for(int i=0;i<data.size();i++){
                    BillObject bil = (BillObject) data.get(i);
                    billDetails.add(bil.id+"(patient ID) - "+bil.name+" - "+bil.bill_id+"(bill ID) - "+bil.total+" - "+bil.date+"\n");
                }
                document.add(billDetails);
            }
            
            document.close();
//            clear catched data
            data = FXCollections.observableArrayList();
            con.disconnect();
            JOptionPane.showMessageDialog(null, "Report successfully saved in directory:\n"+path);
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    public void saveAllBackup(){
        try{
//            choose directory
            DirectoryChooser dc = new DirectoryChooser();
            dc.setTitle("Save report");
            String path = dc.showDialog(null).getAbsolutePath();
            
//            dump sql database
            new Thread(new Task() {
                @Override
                protected Object call() throws Exception {
                    DBConnection db = new DBConnection();
                    db.dump(path);
                    return null;
                }
            }).start();
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
    }
}

class BillObject{
    int id;
    String name;
    int bill_id;
    int total;
    String date;
    
    BillObject(int id, String name, int bill_id, int total, String date){
        this.id = id;
        this.name = name;
        this.bill_id = bill_id;
        this.date = date;
    }
}

class DoctorObject{
    int e_id;
    int d_id;
    String name;
    String last_name;
    String ward_name;
    int count;
    
    DoctorObject(int e_id, int d_id, String name, String last_name, String ward_name, int count){
        this.e_id = e_id;
        this.d_id = d_id;
        this.name = name;
        this.last_name = last_name;
        this.ward_name = ward_name;
        this.count = count;
    }
}

class IncidentObject{
    
    int patientId;
    int incidentId;
    String name;
    String fatherName;
    String date;
    
    IncidentObject(int patientId, int incidentId, String name, String fatherName, String date){
        this.patientId = patientId;
        this.incidentId = incidentId;
        this.name = name;
        this.fatherName = fatherName;
        this.date = date;
    }
}
