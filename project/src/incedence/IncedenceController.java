package incedence;

import custom_utilities.DBConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class IncedenceController implements Initializable {

    @FXML
    private TextField patID;
    @FXML
    private ChoiceBox nurse;
    @FXML
    private ChoiceBox ward;
    @FXML
    private TextArea note;
    @FXML
    private Label patName;
    @FXML
    private Label operationMessage;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        operationMessage.setText("");
        updateNurses();
        updateWard();
    }    
    
    private void updateNurses(){
            //        load nurses
            new Thread(new Task() {
                @Override
                protected Object call() throws Exception {
                    DBConnection db = new DBConnection();
                    String query = "select N.id, E.name, E.last_name from nurse N join employee E on N.employee_id = E.id";
                    PreparedStatement ps = db.con.prepareStatement(query);
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        nurse.getItems().add(rs.getInt("id")+": "+rs.getString("name")+" "+rs.getString("last_name"));
                    }
                    db.disconnect();
                    ps.close();
                    rs.close();
                    return null;
                }
            }).start();
    }
    
    private void updateWard(){
        //            load wards
            new Thread(new Task() {
                @Override
                protected Object call() throws Exception {
                    DBConnection db = new DBConnection();
                    String query = "select * from ward";
                    PreparedStatement ps = db.con.prepareStatement(query);
                    ResultSet rs = ps.executeQuery();
                    while(rs.next()){
                        ward.getItems().add(rs.getInt("id")+": "+rs.getString("name"));
                    }
                    db.disconnect();
                    ps.close();
                    rs.close();
                    return null;
                }
            }).start();
    }
    
    public void updateIncidentLinkHandler()throws Exception{
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("updateIncident.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Update incident");
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void saveIncedenceHandler(){
        try{
             if(ward.getSelectionModel().getSelectedItem() != null & nurse.getSelectionModel().getSelectedItem() != null & !patID.getText().equals("") & ! note.getText().equals("")){
             String query = "insert into incident (date, nurse, patient_id, note) values(now(), ?, ?, ?)";
             DBConnection db = new DBConnection();
            PreparedStatement ps = db.con.prepareStatement(query);
            ps.setInt(1, getIdFromText(nurse.getSelectionModel().getSelectedItem().toString()));
            ps.setInt(2, Integer.parseInt(patID.getText()));
            ps.setString(3, note.getText());
            ps.executeUpdate();
            ps.close();
            db.disconnect();
            operationMessage.setText("Operation successful!");
        }
             else{ JOptionPane.showMessageDialog(null, "Please fill all the blanks");
             }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    private int getIdFromText(String text){
        String arr[] = text.split(": ", 2);
        return Integer.parseInt(arr[0]);
    }
    
//    for the sake of speed and performance
    
    
    
    
    public void patIDvalidation(){
        try{
            DBConnection dbS = new DBConnection();
            String queryS = "select name from patient where id = ?";
            PreparedStatement psS = dbS.con.prepareStatement(queryS);
            psS.setInt(1, Integer.parseInt(patID.getText()));
            ResultSet rsS = psS.executeQuery();
            if(!rsS.next()){
                patName.setText("No record found!");
                patName.setStyle("-fx-text-fill: #ff0b0b");
            }
            else{
                patName.setText(rsS.getString("name"));
                patName.setStyle("-fx-text-fill: #09c92f");
            }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
    }
}
