package calculator;

public class Calculator extends javax.swing.JFrame {

    char operation;
    double firstOperand;
    double secondOperand;
    double result;
    
    public Calculator() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        formula = new javax.swing.JTextField();
        Bone = new javax.swing.JButton();
        Btwo = new javax.swing.JButton();
        Bthree = new javax.swing.JButton();
        Bfour = new javax.swing.JButton();
        Bfive = new javax.swing.JButton();
        Bsix = new javax.swing.JButton();
        Bseven = new javax.swing.JButton();
        Beight = new javax.swing.JButton();
        Bnine = new javax.swing.JButton();
        Bzero = new javax.swing.JButton();
        Bdivid = new javax.swing.JButton();
        Bmultiply = new javax.swing.JButton();
        Bsubtract = new javax.swing.JButton();
        Badd = new javax.swing.JButton();
        Bequal = new javax.swing.JButton();
        Bclear = new javax.swing.JButton();
        Lerror = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        formula.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        Bone.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Bone.setText("1");
        Bone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BoneActionPerformed(evt);
            }
        });

        Btwo.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Btwo.setText("2");
        Btwo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtwoActionPerformed(evt);
            }
        });

        Bthree.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Bthree.setText("3");
        Bthree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BthreeActionPerformed(evt);
            }
        });

        Bfour.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Bfour.setText("4");
        Bfour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BfourActionPerformed(evt);
            }
        });

        Bfive.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Bfive.setText("5");
        Bfive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BfiveActionPerformed(evt);
            }
        });

        Bsix.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Bsix.setText("6");
        Bsix.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BsixActionPerformed(evt);
            }
        });

        Bseven.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Bseven.setText("7");
        Bseven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BsevenActionPerformed(evt);
            }
        });

        Beight.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Beight.setText("8");
        Beight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BeightActionPerformed(evt);
            }
        });

        Bnine.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Bnine.setText("9");
        Bnine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BnineActionPerformed(evt);
            }
        });

        Bzero.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Bzero.setText("0");
        Bzero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BzeroActionPerformed(evt);
            }
        });

        Bdivid.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        Bdivid.setText("/");
        Bdivid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BdividActionPerformed(evt);
            }
        });

        Bmultiply.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        Bmultiply.setText("x");
        Bmultiply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BmultiplyActionPerformed(evt);
            }
        });

        Bsubtract.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        Bsubtract.setText("-");
        Bsubtract.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BsubtractActionPerformed(evt);
            }
        });

        Badd.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        Badd.setText("+");
        Badd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BaddActionPerformed(evt);
            }
        });

        Bequal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        Bequal.setText("=");
        Bequal.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                BequalFocusLost(evt);
            }
        });
        Bequal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BequalActionPerformed(evt);
            }
        });

        Bclear.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        Bclear.setText("C");
        Bclear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BclearActionPerformed(evt);
            }
        });

        Lerror.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Lerror.setForeground(new java.awt.Color(255, 51, 51));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(formula)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Bone, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Btwo, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Bthree, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Bfour, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Bfive, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Bsix, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Bdivid, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Bmultiply, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Bsubtract, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Bseven, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Beight, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Bnine, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Bclear, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Bzero, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Badd, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Bequal, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(Lerror, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(formula, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Lerror)
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Bone, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Btwo, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Bthree, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Bfour, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Bfive, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Bsix, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Bseven, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Beight, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Bnine, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Bzero, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Bclear, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Bdivid, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Bmultiply, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Bsubtract, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Badd, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Bequal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(73, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BclearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BclearActionPerformed
        formula.setText("");
    }//GEN-LAST:event_BclearActionPerformed

    private void BoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BoneActionPerformed
        formula.setText(formula.getText()+"1");
    }//GEN-LAST:event_BoneActionPerformed

    private void BtwoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtwoActionPerformed
        formula.setText(formula.getText()+"2");
    }//GEN-LAST:event_BtwoActionPerformed

    private void BthreeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BthreeActionPerformed
        formula.setText(formula.getText()+"3");
    }//GEN-LAST:event_BthreeActionPerformed

    private void BfourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BfourActionPerformed
        formula.setText(formula.getText()+"4");
    }//GEN-LAST:event_BfourActionPerformed

    private void BfiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BfiveActionPerformed
        formula.setText(formula.getText()+"5");
    }//GEN-LAST:event_BfiveActionPerformed

    private void BsixActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BsixActionPerformed
        formula.setText(formula.getText()+"6");
    }//GEN-LAST:event_BsixActionPerformed

    private void BsevenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BsevenActionPerformed
        formula.setText(formula.getText()+"7");
    }//GEN-LAST:event_BsevenActionPerformed

    private void BeightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BeightActionPerformed
        formula.setText(formula.getText()+"8");
    }//GEN-LAST:event_BeightActionPerformed

    private void BnineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BnineActionPerformed
        formula.setText(formula.getText()+"9");
    }//GEN-LAST:event_BnineActionPerformed

    private void BzeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BzeroActionPerformed
        formula.setText(formula.getText()+"0");
    }//GEN-LAST:event_BzeroActionPerformed

    private void BdividActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BdividActionPerformed
        try{
            firstOperand = (double) Integer.parseInt(formula.getText());
            operation = '/';
            formula.setText("");
        }
        catch(Exception e){
            Lerror.setText("Invalid input!");
        }
    }//GEN-LAST:event_BdividActionPerformed

    private void BmultiplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BmultiplyActionPerformed
        try{
            firstOperand = (double) Integer.parseInt(formula.getText());
            operation = 'x';
            formula.setText("");
        }
        catch(Exception e){
            Lerror.setText("Invalid input!");
        }
    }//GEN-LAST:event_BmultiplyActionPerformed

    private void BsubtractActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BsubtractActionPerformed
        try{
            firstOperand = (double) Integer.parseInt(formula.getText());
            operation = '-';
            formula.setText("");
        }
        catch(Exception e){
            Lerror.setText("Invalid input!");
        }
    }//GEN-LAST:event_BsubtractActionPerformed

    private void BaddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BaddActionPerformed
        try{
            firstOperand = (double) Integer.parseInt(formula.getText());
            operation = '+';
            formula.setText("");
        }
        catch(Exception e){
            Lerror.setText("Invalid input!");
        }
    }//GEN-LAST:event_BaddActionPerformed

    private void BequalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BequalActionPerformed
        try{
            secondOperand = (double) Integer.parseInt(formula.getText());
            switch(operation){
                case '/':
                    try{
                        result = firstOperand / secondOperand;
                    }
                    catch(Exception e){
                        Lerror.setText("Division on zero!");
                    }
                    break;
                case 'x':
                    result = firstOperand * secondOperand;
                    break;
                case '-':
                    result = firstOperand - secondOperand;
                    break;
                case '+':
                    result = firstOperand + secondOperand;
                    break;
            }
            formula.setText(Double.toString(result));
        }
        catch(Exception e){
                
        }
    }//GEN-LAST:event_BequalActionPerformed

    private void BequalFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_BequalFocusLost
        formula.setText("");
    }//GEN-LAST:event_BequalFocusLost

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calculator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calculator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calculator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calculator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Calculator().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Badd;
    private javax.swing.JButton Bclear;
    private javax.swing.JButton Bdivid;
    private javax.swing.JButton Beight;
    private javax.swing.JButton Bequal;
    private javax.swing.JButton Bfive;
    private javax.swing.JButton Bfour;
    private javax.swing.JButton Bmultiply;
    private javax.swing.JButton Bnine;
    private javax.swing.JButton Bone;
    private javax.swing.JButton Bseven;
    private javax.swing.JButton Bsix;
    private javax.swing.JButton Bsubtract;
    private javax.swing.JButton Bthree;
    private javax.swing.JButton Btwo;
    private javax.swing.JButton Bzero;
    private javax.swing.JLabel Lerror;
    private javax.swing.JTextField formula;
    // End of variables declaration//GEN-END:variables
}
