package generalization;

public class Main {
    public static void main(String[] args) {
        CustomeType c = new CustomeType();
        c.add("ehsan");
        c.add(10);
        c.add("popal");
        c.print();
        c.isEqual("ehsan", "ehsan");
    }
}

class CustomeType<T>{
    private T[] data;
//    print elements in dynamic array
    public void print(){
        for(T i : data){
            System.out.println(i);
        }
    }
//    insert a new elements to dynamic array
    public void add(T t){
        if(data == null){
            data = (T[]) new Object[1];
            data[0] = t;
        }
        else{
            T[] temp = (T[]) new Object[data.length+1];
            data = copy(data, temp);
            data[data.length-1] = t;
        }
    }
//    copy elements from one array to another
    private T[] copy(T[] source, T[] distination){
        for(int i=0; i<source.length;i++){
            distination[i] = source[i];
        }
        return distination;
    }
//    comparison checker
    public void isEqual(T first, T second){
        String firstClassName = getClassName(first.getClass().getTypeName());
        String secondClassName = getClassName(second.getClass().getTypeName());
        
        if(firstClassName.equals(secondClassName)){
            if(first == second){
                System.out.println("equal");
            }
            else{
                System.out.println("not equal");
            }
        }
        else{
            System.out.println("incomparable data types!");
        }
    }
//    get class name from java.lang.class_name string
    private String getClassName(String string){
        String result = "";
        for(int i=10;i<string.length();i++){
            result = result+string.charAt(i);
        }
        return result;
    }
}

// code for integer sorting only

//class CustomType{
//    public int[] sort(int a[]){
//        if(a.length > 1){
//            for(int j=0;j<a.length;j++){
//                for(int i=0;i<a.length-1;i++){
//                    if(a[i]>a[i+1]){
//                        int temp = a[i];
//                        a[i] = a[i+1];
//                        a[i+1] = temp;
//                    }
//                }   
//            }
//        }
//        return a;
//    }
//    public void print(int list[]){
//        for(int i : list){
//            System.out.println(i);
//        }
//    }
//}
